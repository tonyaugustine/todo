package com.smirk.todo.di

import android.app.Application
import androidx.room.Room
import com.smirk.todo.db.AppDataBase
import com.smirk.todo.db.daos.ToDoDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Tony Augustine on 19,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
class RoomModule {

    @Singleton
    @Provides
    fun provideAppDataBase(context: Application): AppDataBase {
        return Room.databaseBuilder(context, AppDataBase::class.java, "todo_db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    fun provideToDoDao(appDataBase: AppDataBase): ToDoDao {
        return appDataBase.toDoDao()
    }

}