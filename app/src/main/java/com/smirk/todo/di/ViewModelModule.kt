package com.smirk.todo.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.smirk.todo.viewmodel.ViewModelFactory
import com.smirk.todo.viewmodel.ViewModelTodo
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ViewModelTodo::class)
    abstract fun bindTodoViewModel(todoModel : ViewModelTodo) : ViewModel
}