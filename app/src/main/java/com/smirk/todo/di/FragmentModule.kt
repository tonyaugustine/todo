package com.smirk.todo.di

import com.smirk.todo.ui.detail.ToDoDetailFragment
import com.smirk.todo.ui.list.AddToDoFragment
import com.smirk.todo.ui.list.ListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Tony Augustine on 29,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributeListFragment() : ListFragment

    @ContributesAndroidInjector
    abstract fun contributeDetailFragment() : ToDoDetailFragment

    @ContributesAndroidInjector
    abstract fun contributeAddFragment() : AddToDoFragment
}