package com.smirk.todo.di

import com.smirk.todo.api.ApiService
import com.smirk.todo.api.BASE_URL
import com.smirk.todo.utils.LiveDataCallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
@Module(includes = [OkHttpClientModule::class])
class RetrofitModule {

    @Singleton
    @Provides
    fun provideApiService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofit(
        httpClient: OkHttpClient,
        gsonConverterFactory: GsonConverterFactory,
        liveDataAdapterFactory: LiveDataCallAdapterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(liveDataAdapterFactory)
            .build()
    }

    @Singleton
    @Provides
    fun provideLiveDataAdapterFactory(): LiveDataCallAdapterFactory {
        return LiveDataCallAdapterFactory()
    }

    @Singleton
    @Provides
    fun provideGsonConvertorFactoty(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

}