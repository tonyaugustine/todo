package com.smirk.todo.di

import com.smirk.todo.ui.SplashActivity
import com.smirk.todo.ui.list.ListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
@Module
abstract class ActivitiesModule {

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeSplashActivity() : SplashActivity

    @ContributesAndroidInjector(modules = [FragmentModule::class])
    abstract fun contributeListActivity() : ListActivity

}