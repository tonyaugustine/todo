package com.smirk.todo.api

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */

const val BASE_URL: String = "https://bboxx-android-coding-challenge.herokuapp.com/"

const val TODOLIST = "todos.json"
const val NEW_TODO = "todos.json"
const val PATCH_TODO = "todos/{id}.json"
const val DELETE_TODO = "todos/{id}.json"