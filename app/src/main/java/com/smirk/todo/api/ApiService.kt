package com.smirk.todo.api

import androidx.lifecycle.LiveData
import com.smirk.todo.model.list.NewToDo
import com.smirk.todo.model.list.ResponseTodo
import retrofit2.http.*

/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */
interface ApiService {

    @GET(TODOLIST)
    fun getToDoList() : LiveData<ApiResponse<List<ResponseTodo>>>

    @POST(NEW_TODO)
    fun createToDo(@Body body: NewToDo) : LiveData<ApiResponse<ResponseTodo>>

    @PATCH(PATCH_TODO)
    fun updateToDo(@Path("id") id : String, @Body body: NewToDo) : LiveData<ApiResponse<ResponseTodo>>

    @DELETE(DELETE_TODO)
    fun deleteToDo(@Path("id") id : String) : LiveData<ApiResponse<Any>>
}