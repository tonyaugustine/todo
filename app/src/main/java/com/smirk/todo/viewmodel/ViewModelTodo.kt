package com.smirk.todo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.smirk.todo.model.list.NewToDo
import com.smirk.todo.model.list.ResponseTodo
import com.smirk.todo.repository.TodoRepo
import com.smirk.todo.utils.AbsentLiveData
import com.smirk.todo.vo.Resource
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by Tony Augustine on 21,November,2019
 * tonyaugustine47@gmail.com
 */
class ViewModelTodo
@Inject constructor(repo: TodoRepo) : ViewModel() {

    private val toDoList = MutableLiveData<String>()

    fun changeToDoList(data : String) {
        toDoList.value = data
    }

    val getToDoList: LiveData<Resource<List<ResponseTodo>>> = Transformations.switchMap(toDoList) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.getToDoList()
        }
    }

    private val newToDo = MutableLiveData<NewToDo>()

    fun createToDo(data : NewToDo) {
        newToDo.value = data
    }

    val newToDoCreate: LiveData<Resource<ResponseTodo>> = Transformations.switchMap(newToDo) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.createToDo(it)
        }
    }

    private var date: String = ""

    fun setOriginalDateFormat(dateOriginal : String) {
        this.date = dateOriginal
    }

    fun getDateOriginalFormat() : String {
        return date
    }

    val changeFragment = MutableLiveData<ResponseTodo>()

    fun setFragmentChange(data : ResponseTodo) {
        changeFragment.value = data
    }

    private val popBackStack = MutableLiveData<String>()

    fun setPopStack(data : String) {
        popBackStack.value = data
    }

    fun getPopStack() : LiveData<String> {
        return popBackStack
    }

    private var toDoId = ""

    fun setTodoId(id : String) {
        toDoId = id
    }

    private val updateObserve = MutableLiveData<NewToDo>()

    fun updateData(data : NewToDo) {
        updateObserve.value = data
    }

    val updatedTodo : LiveData<Resource<ResponseTodo>> = Transformations.switchMap(updateObserve) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.updateToDo(toDoId, it)
        }
    }

    private val updateStatus = MutableLiveData<NewToDo>()

    fun updateStatus(data : NewToDo) {
        updateStatus.value = data
    }

    val updatedStatus : LiveData<Resource<ResponseTodo>> = Transformations.switchMap(updateStatus) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.updateToDo(toDoId, it)
        }
    }

    private val deleteObserve = MutableLiveData<String>()

    fun deleteData() {
        deleteObserve.value = toDoId
    }

    val deleteToDo : LiveData<Resource<Any>> = Transformations.switchMap(deleteObserve) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.deleteToDo(it)
        }
    }

    private val sortData = MutableLiveData<String>()

    fun sortData() {
        sortData.value = "sort"
    }

    val sortedData : LiveData<Resource<List<ResponseTodo>>> = Transformations.switchMap(sortData) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.getSortedListName()
        }
    }

    private val sortStatus = MutableLiveData<String>()

    fun sortDataStatus() {
        sortStatus.value = "Completed"
    }

    val sortedStatus : LiveData<Resource<List<ResponseTodo>>> = Transformations.switchMap(sortStatus) {
        if (it == null) {
            AbsentLiveData.create()
        }else {
            repo.getSortedListStatus(it)
        }
    }

}