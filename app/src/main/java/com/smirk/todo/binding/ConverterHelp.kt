package com.smirk.todo.binding

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by Smirk_1120 on 11,November,2018
 * tonyaugustine47@gmail.com
 */

inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)