package com.smirk.todo.binding

import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.fragment.app.Fragment
import javax.inject.Inject
import com.squareup.picasso.Picasso



/**
 * Created by Tony Augustine on 09,September,2019
 * tonyaugustine47@gmail.com
 */

class FragmentBindingAdapters @Inject constructor(val fragment : Fragment) {

    @BindingAdapter("imageUrl")
    fun bindImage(imageView: ImageView, url: String?) {

        Picasso.get().load(url).into(imageView)

    }

    @BindingAdapter("app:visibility")
    fun viewVisibility(view : View, isVisible : Boolean) {
        if (isVisible) {
            view.visibility = View.VISIBLE
        }else {
            view.visibility = View.GONE
        }
    }

    @BindingAdapter("app:status")
    fun setStatus(view : View, status : String) {
        if (status.equals("Completed", true)) {
            (view as TextView).text = status
            view.visibility = View.VISIBLE
        }else {
            view.visibility = View.GONE
        }
    }
}