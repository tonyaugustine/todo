package com.smirk.todo.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.smirk.todo.db.daos.ToDoDao
import com.smirk.todo.model.list.ResponseTodo

/**
 * Created by Tony Augustine on 19,September,2019
 * tonyaugustine47@gmail.com
 */
@Database(entities = [ResponseTodo::class], version = 1, exportSchema = false)
abstract class AppDataBase : RoomDatabase() {

    abstract fun toDoDao() : ToDoDao
}