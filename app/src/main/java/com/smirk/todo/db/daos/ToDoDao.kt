package com.smirk.todo.db.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.smirk.todo.model.list.ResponseTodo

/**
 * Created by Tony Augustine on 21,November,2019
 * tonyaugustine47@gmail.com
 */
@Dao
abstract class ToDoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insertToDoList(list : List<ResponseTodo>)

    @Query("SELECT * FROM ResponseTodo")
    abstract fun getTodoList() : LiveData<List<ResponseTodo>>

    @Query("SELECT * FROM ResponseTodo ORDER BY name ASC")
    abstract fun getSortedListByName() : LiveData<List<ResponseTodo>>

    @Query("SELECT * FROM ResponseTodo WHERE status = :status_")
    abstract fun getSortedListByStatus(status_ : String) : LiveData<List<ResponseTodo>>

    @Query("DELETE FROM ResponseTodo")
    abstract fun clearTable()

}