package com.smirk.todo.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Tony Augustine on 21,November,2019
 * tonyaugustine47@gmail.com
 */
object Utils {

    fun getConvertedDate(data : String?) : String {
        if (data == null)
            return ""

        val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val formatConvert = SimpleDateFormat("dd MMM-yyyy", Locale.ENGLISH)
        val date = format.parse(data)

        val convertedDate = formatConvert.format(date)

        return convertedDate
    }
}