package com.smirk.todo.model.list

import com.google.gson.annotations.SerializedName

/**
 * Created by Tony Augustine on 21,November,2019
 * tonyaugustine47@gmail.com
 */
class NewToDo {
    var expiry_date: String? = null
    @SerializedName("name")
    var name_: String? = null
    var description: String? = null
    var status: String? = null
}