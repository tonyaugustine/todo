package com.smirk.todo.model.list

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class ResponseTodo(
	val updated_at: String? = null,
	val expiry_date: String? = null,
	val name: String? = null,
	val description: String? = null,
	val created_at: String? = null,
	@PrimaryKey
	val id: Int? = null,
	val url: String? = null,
	val status: String? = null
) : Serializable
