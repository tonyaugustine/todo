package com.smirk.todo.base

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.smirk.todo.AppExecutors

/**
 * A generic RecyclerView adapter that uses Data Binding & DiffUtil.
 *
 * @param <T> Type of the items in the list
 * @param <V> The type of the ViewDataBinding
</V></T> */
abstract class DataBoundListAdapterType<T, V : ViewDataBinding>(appExecutors: AppExecutors, diffCallback: DiffUtil.ItemCallback<T>
) : BaseDataBindListAdapterWithPosition<T, V>(appExecutors, diffCallback) {


    override fun getItemViewType(position: Int): Int {
        return viewType(getItem(position))
    }

    protected abstract fun viewType(item : T) : Int

}