package com.smirk.todo.ui.list

import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.smirk.todo.R
import com.smirk.todo.di.Injectable
import com.smirk.todo.model.list.NewToDo
import com.smirk.todo.viewmodel.ViewModelFactory
import com.smirk.todo.viewmodel.ViewModelTodo
import kotlinx.android.synthetic.main.fragment_add_todo.*
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import javax.xml.datatype.DatatypeConstants.MONTHS
import kotlin.collections.HashMap

/**
 * Created by Tony Augustine on 21,November,2019
 * tonyaugustine47@gmail.com
 */

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class AddToDoFragment : BottomSheetDialogFragment(), Injectable {

    private var param1: String? = null
    private var param2: String? = null

    lateinit var activity: ListActivity
    lateinit var viewModel : ViewModelTodo

    @Inject
    lateinit var  mViewModelFactory: ViewModelFactory

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            AddToDoFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as ListActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_add_todo, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel = ViewModelProviders.of(activity, mViewModelFactory).get(ViewModelTodo::class.java)

        dateIcon.setOnClickListener {
            showDatePickerdialog()
        }

        save.setOnClickListener {

            val data = NewToDo().apply {
                name_ = name.text.toString()
                description = desc.text.toString()
                expiry_date = viewModel.getDateOriginalFormat()
                status = "Due"
            }

            viewModel.createToDo(data)
            dismiss()
        }

    }

    private fun showDatePickerdialog() {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(activity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            date.visibility = View.VISIBLE
            val month = monthOfYear+1
            val mDate = "$year-$month-$dayOfMonth"
            date.text = getConvertedDate(mDate)
            viewModel.setOriginalDateFormat(mDate)

        }, year, month, day)

        dpd.show()
    }

    private fun getConvertedDate(data : String) : String {
        val format = SimpleDateFormat("yyyy-MM-dd")
        val formatConvert = SimpleDateFormat("dd MMM yyyy")
        val date = format.parse(data)
        val convertedDate = formatConvert.format(date)

        return convertedDate
    }
    
}