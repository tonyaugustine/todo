package com.smirk.todo.ui.list


import android.graphics.*
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.smirk.todo.AppExecutors

import com.smirk.todo.R
import com.smirk.todo.base.BaseFragment
import com.smirk.todo.databinding.FragmentListBinding
import com.smirk.todo.model.list.NewToDo
import com.smirk.todo.model.list.ResponseTodo
import com.smirk.todo.utils.autoCleared
import com.smirk.todo.viewmodel.ViewModelTodo
import kotlinx.android.synthetic.main.fragment_list.*
import javax.inject.Inject

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ListFragment : BaseFragment<FragmentListBinding>(), AdapterToDoList.Listeners {
    private var param1: String? = null
    private var param2: String? = null

    private var adapterToDoList by autoCleared<AdapterToDoList>()

    @Inject
    lateinit var appExecutors : AppExecutors
    private lateinit var viewModel : ViewModelTodo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        uiInit()

        fabCreate.setOnClickListener {
            val dialog = AddToDoFragment.newInstance("", "")
            dialog.show(childFragmentManager, "addTodo")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let {
            viewModel = getViewModelShared(it, ViewModelTodo::class.java)
        }
        observers()

        viewModel.changeToDoList("toDoList")
    }

    fun uiInit() {
        adapterToDoList = AdapterToDoList(dataBindingComponent, appExecutors)
        adapterToDoList.setListener(this)

        toDoList.adapter = adapterToDoList

        val simpleItemTouchCallback =
            object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    val position = viewHolder.adapterPosition
                    if (direction == ItemTouchHelper.RIGHT) {
                        val deletedModel = adapterToDoList.getCurrentItem(position)
                        adapterToDoList.removeItem(position)
                        // showing snack bar with Undo option
                        val snackbar = Snackbar.make(
                            activity!!.window.decorView.rootView,
                            "Deleted",
                            Snackbar.LENGTH_LONG
                        )
                        snackbar.show()
                        viewModel.setTodoId(deletedModel.id.toString())
                        viewModel.deleteData()
                    }
                }

                override fun onChildDraw(
                    c: Canvas,
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    dX: Float,
                    dY: Float,
                    actionState: Int,
                    isCurrentlyActive: Boolean
                ) {

                    val icon: Bitmap
                    val p = Paint()
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                        val itemView = viewHolder.itemView
                        val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                        val width = height / 3

                        if (dX > 0) {
                            p.color = Color.parseColor("#388E3C")
                            val background =
                                RectF(itemView.left.toFloat(), itemView.top.toFloat(), dX, itemView.bottom.toFloat())
                            c.drawRect(background, p)
                            icon = BitmapFactory.decodeResource(resources, R.drawable.baseline_delete_white_24dp)
                            val icon_dest = RectF(
                                itemView.left.toFloat() + width,
                                itemView.top.toFloat() + width,
                                itemView.left.toFloat() + 2 * width,
                                itemView.bottom.toFloat() - width
                            )
                            c.drawBitmap(icon, null, icon_dest, p)
                        }
                    }

                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
                }
            }
        val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
        itemTouchHelper.attachToRecyclerView(toDoList)
    }

    private fun observers() {

        viewModel.getToDoList.observe(this, Observer {response->
            if (response.data == null)
                return@Observer
            adapterToDoList.setData(response.data)
//            adapterToDoList.submitList(response.data)
        })

        viewModel.newToDoCreate.observe(this, Observer {
            if (it.data == null)
                return@Observer

            viewModel.changeToDoList("toDoList")
            Toast.makeText(activity, "Created", Toast.LENGTH_LONG).show()
        })

        viewModel.updatedStatus.observe(this, Observer {
            viewModel.changeToDoList("toDoList")
        })

        viewModel.sortedData.observe(this, Observer {
            if (it.data == null)
                return@Observer
            adapterToDoList.setData(it.data)
            adapterToDoList.notifyData()
        })

        viewModel.deleteToDo.observe(this, Observer {
            if (it.data == null)
                return@Observer

            viewModel.changeToDoList("toDoList")
        })

        viewModel.sortedStatus.observe(this, Observer {
            if (it.data == null)
                return@Observer

            adapterToDoList.setData(it.data)
            adapterToDoList.notifyData()
        })
    }

    override fun changeFagment(data: ResponseTodo) {
        viewModel.setFragmentChange(data)
    }

    override fun statusChange(data: ResponseTodo, isCompleted: Boolean) {
        viewModel.setTodoId(data.id.toString())
        val dataChange = NewToDo().apply {
            name_ = data.name
            description = data.description
            expiry_date = data.expiry_date
            status = if (isCompleted)"Completed" else "Due"
        }
        viewModel.updateStatus(dataChange)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.sort, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        when(id) {
            R.id.sort -> {
                viewModel.sortData()
            }

            R.id.completed -> {
                viewModel.sortDataStatus()
            }

        }

        return true
    }


}
