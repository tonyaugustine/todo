package com.smirk.todo.ui

import android.content.Intent
import android.os.Bundle
import com.smirk.todo.base.BaseActivity
import com.smirk.todo.ui.list.ListActivity

class SplashActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, ListActivity::class.java)
        startActivity(intent)
        finish()
    }
}
