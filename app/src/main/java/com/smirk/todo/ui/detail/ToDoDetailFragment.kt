package com.smirk.todo.ui.detail


import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import com.smirk.todo.AppExecutors

import com.smirk.todo.R
import com.smirk.todo.base.BaseFragment
import com.smirk.todo.databinding.FragmentToDoDetailBinding
import com.smirk.todo.model.list.ResponseTodo
import com.smirk.todo.utils.Utils
import com.smirk.todo.viewmodel.ViewModelTodo
import kotlinx.android.synthetic.main.fragment_add_todo.date
import kotlinx.android.synthetic.main.fragment_to_do_detail.*
import java.util.*
import javax.inject.Inject
import kotlin.collections.HashMap
import androidx.lifecycle.Observer
import com.smirk.todo.model.list.NewToDo
import com.smirk.todo.utils.AbsentLiveData
import com.smirk.todo.vo.Resource
import com.smirk.todo.vo.Status
import kotlin.math.exp


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ToDoDetailFragment : BaseFragment<FragmentToDoDetailBinding>() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    @Inject
    lateinit var appExecutors : AppExecutors
    private lateinit var viewModel : ViewModelTodo

    private var isUpdate = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    companion object {
        @JvmStatic
        fun newInstance(response : ResponseTodo) =
            ToDoDetailFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_PARAM1, response)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun getLayoutId(): Int {
       return R.layout.fragment_to_do_detail
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        when(id) {
            R.id.save -> {
                updateData()
            }

            R.id.delete -> {
                viewModel.deleteData()
            }
        }

        return true
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.let {

            viewModel = getViewModelShared(it, ViewModelTodo::class.java)
        }
        initUi()

        var resposne : ResponseTodo? = null

        arguments?.let {
            resposne = it.getSerializable(ARG_PARAM1) as ResponseTodo?
        }

        if (resposne == null)
            return

        mBinding.data = resposne

        viewModel.setTodoId(resposne?.id.toString())

        if (resposne?.status.equals("Completed", true)) {
            mBinding.complete.visibility = View.GONE
        }

        if (resposne?.expiry_date != null) {
            viewModel.setOriginalDateFormat(resposne!!.expiry_date!!)
            mBinding.date.text = Utils.getConvertedDate(resposne?.expiry_date)
        }

        mBinding.date.setOnClickListener {
            showDatePickerdialog()
        }

    }

    private fun initUi() {
        viewModel.updatedTodo.removeObservers(this)

       viewModel.updatedTodo.observe(viewLifecycleOwner, Observer {response->
           if (response.data == null || response.status == Status.LOADING)
               return@Observer
           if (isUpdate) {
               viewModel.changeToDoList("toDoList")
               viewModel.setPopStack("popStack")
               isUpdate = false
           }
       })
        viewModel.deleteToDo.removeObservers(this)
        viewModel.deleteToDo.observe(viewLifecycleOwner, Observer {response ->
            if (response.data == null || response.status == Status.LOADING)
                return@Observer

            viewModel.changeToDoList("toDoList")
            viewModel.setPopStack("popStack")

        })

        viewModel.updatedStatus.removeObservers(this)
        viewModel.updatedStatus.observe(this, Observer {
            if (it.data == null || it.status == Status.LOADING)
                return@Observer

            if (isUpdate) {
                isUpdate = false
                viewModel.changeToDoList("toDoList")
                viewModel.setPopStack("popStack")
            }

        })

        complete.setOnClickListener {
            changeStatus()
        }
    }

    private fun changeStatus() {
        isUpdate = true
        val data = NewToDo().apply {
            name_ = name.text.toString()
            description = desc.text.toString()
            expiry_date = viewModel.getDateOriginalFormat()
            status = "Completed"
        }
        viewModel.updateStatus(data)

    }

    private fun updateData() {
       /* val data = HashMap<String, String>()
        data["name"] = name.text.toString()
        data["description"] = desc.text.toString()
        data["expiry_date"] = viewModel.getDateOriginalFormat()*/

        isUpdate = true

        val data = NewToDo().apply {
            name_ = name.text.toString()
            description = desc.text.toString()
            expiry_date = viewModel.getDateOriginalFormat()
            status = "Due"
        }
        viewModel.updateData(data)
    }

    private fun showDatePickerdialog() {

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = activity?.let {
            DatePickerDialog(
                it,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                    date.visibility = View.VISIBLE
                    val month = monthOfYear + 1
                    val mDate = "$year-$month-$dayOfMonth"
                    viewModel.setOriginalDateFormat(mDate)
                    date.text = Utils.getConvertedDate(mDate)

                },
                year,
                month,
                day
            )
        }
        dpd?.show()
    }
}
