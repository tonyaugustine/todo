package com.smirk.todo.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import com.smirk.todo.AppExecutors
import com.smirk.todo.R
import com.smirk.todo.base.BaseDataBindListAdapterWithPosition
import com.smirk.todo.databinding.InflateTodoBinding
import com.smirk.todo.model.list.ResponseTodo
import com.smirk.todo.utils.Utils
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AdapterToDoList(val dataBindingComponent: DataBindingComponent, appExecutors: AppExecutors) :
BaseDataBindListAdapterWithPosition<ResponseTodo, InflateTodoBinding>(appExecutors = appExecutors,
    diffCallback = object : DiffUtil.ItemCallback<ResponseTodo>() {
        override fun areItemsTheSame(oldItem: ResponseTodo, newItem: ResponseTodo): Boolean {
          return false
        }

        override fun areContentsTheSame(oldItem: ResponseTodo, newItem: ResponseTodo): Boolean {
            return oldItem.updated_at.equals(newItem.updated_at)
        }

    }){


    override fun createBinding(parent: ViewGroup, viewType: Int): InflateTodoBinding {
        return DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.inflate_todo, parent,
            false, dataBindingComponent)
    }

    private lateinit var listeners: Listeners
    fun setListener(callback: Listeners) {
        listeners = callback
    }

    private val listData = ArrayList<ResponseTodo>()

    fun setData(list : List<ResponseTodo>) {
        listData.clear()
        listData.addAll(list)
        submitList(listData)
        notifyData()
    }

    fun notifyData() {
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        listData.removeAt(position)
        submitList(listData)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, currentList.size)
    }

    fun getCurrentItem(position: Int): ResponseTodo {
        return getItem(position)
    }

    fun restoreItem(model: ResponseTodo, position: Int) {
        currentList.add(position, model)
        // notify item added by position
        notifyItemInserted(position)
    }

    override fun bind(binding: InflateTodoBinding, item: ResponseTodo, position : Int) {

        if (item.expiry_date != null) {
            val data = Utils.getConvertedDate(item.expiry_date)
            val splittedData = data.split("-")
            binding.month.text = splittedData[0]
            binding.year.text = splittedData[1]
        }

       binding.todo = item

        binding.root.setOnClickListener {
            binding.todo?.let {
                listeners.changeFagment(item)
            }
        }

        binding.checkBox.setOnCheckedChangeListener(null)
        binding.checkBox.isChecked = item.status.equals("Completed", true)
        binding.checkBox.setOnCheckedChangeListener{compoundButton, isChecked ->
            binding.todo?.let {
                listeners.statusChange(item, isChecked)
            }

        }

    }

    interface Listeners {
        fun changeFagment(data : ResponseTodo)
        fun statusChange(data: ResponseTodo, isCompleted: Boolean)
    }
/*
    private fun getConvertedDate(data : String) : String {
        val format = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val formatConvert = SimpleDateFormat("dd MMM-yyyy", Locale.ENGLISH)
        val date = format.parse(data)

        val convertedDate = formatConvert.format(date)

        return convertedDate
    }*/

}