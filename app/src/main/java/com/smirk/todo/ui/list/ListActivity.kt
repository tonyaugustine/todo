package com.smirk.todo.ui.list

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.view.Menu
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.smirk.todo.R
import com.smirk.todo.base.BaseActivity
import com.smirk.todo.model.list.ResponseTodo
import com.smirk.todo.ui.detail.ToDoDetailFragment
import com.smirk.todo.viewmodel.ViewModelFactory
import com.smirk.todo.viewmodel.ViewModelTodo
import kotlinx.android.synthetic.main.activity_list.*
import javax.inject.Inject

class ListActivity : BaseActivity() {

    lateinit var viewModel: ViewModelTodo

    @Inject
    lateinit var mViewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        uiInit()
        viewModel = ViewModelProviders.of(this, mViewModelFactory).get(ViewModelTodo::class.java)

        changeFragment()
        observers()

    }

    private fun uiInit() {
        setSupportActionBar(toolbar)

        supportFragmentManager.addOnBackStackChangedListener {
            val count = supportFragmentManager.backStackEntryCount
            if (count == 0) {
                setNavigationIcon(false)
            }else {
                setNavigationIcon(true)
            }
        }
    }

    private fun observers() {
        viewModel.changeFragment.observe(this, Observer { response ->
            if (response == null)
                return@Observer

            changeFragment(response)

        })

        viewModel.getPopStack().observe(this, Observer {
            popBackStackFrag()
        })
    }

    fun changeFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ListFragment.newInstance("", ""))
            .commit()
    }

    fun changeFragment(response: ResponseTodo) {
        supportFragmentManager.beginTransaction()
            .add(R.id.container, ToDoDetailFragment.newInstance(response))
            .addToBackStack(null)
            .commit()
    }


    private fun setNavigationIcon(isNeed : Boolean) {
        if (isNeed) {
            toolbar.setNavigationIcon(R.drawable.ic_arrow_back_black_24dp)
            toolbar.setNavigationOnClickListener {
                popBackStackFrag()
            }

        }else {
            toolbar.navigationIcon = null
        }

    }

    private fun popBackStackFrag() {
        if (supportFragmentManager.backStackEntryCount > 0 ) {
            supportFragmentManager.popBackStackImmediate()
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {

        if (ev?.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(
                        Context.INPUT_METHOD_SERVICE
                    ) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.windowToken, 0)
                }
            }
        }

        return super.dispatchTouchEvent(ev)
    }

}
