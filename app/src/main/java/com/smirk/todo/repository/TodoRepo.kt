package com.smirk.todo.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.smirk.todo.AppExecutors
import com.smirk.todo.api.ApiResponse
import com.smirk.todo.api.ApiService
import com.smirk.todo.db.AppDataBase
import com.smirk.todo.model.list.NewToDo
import com.smirk.todo.model.list.ResponseTodo
import com.smirk.todo.vo.Resource
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Tony Augustine on 29,September,2019
 * tonyaugustine47@gmail.com
 */
@Singleton
class TodoRepo @Inject constructor(
    private val appExecutors: AppExecutors,
    private val apiService: ApiService,
    private val db: AppDataBase
) {

    fun getToDoList() : LiveData<Resource<List<ResponseTodo>>> {
        return object : NetworkBoundResource<List<ResponseTodo>, List<ResponseTodo>>(appExecutors) {
            override fun saveCallResult(item: List<ResponseTodo>) {
                db.toDoDao().clearTable()
                db.toDoDao().insertToDoList(item)
            }

            override fun shouldFetch(data: List<ResponseTodo>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<ResponseTodo>> {
                return db.toDoDao().getTodoList()
            }

            override fun createCall(): LiveData<ApiResponse<List<ResponseTodo>>> {
                return apiService.getToDoList()
            }

        }.asLiveData()
    }

    fun getSortedListName() : LiveData<Resource<List<ResponseTodo>>> {
        return object : NetworkBoundResource<List<ResponseTodo>, List<ResponseTodo>>(appExecutors) {
            override fun saveCallResult(item: List<ResponseTodo>) {
                db.toDoDao().insertToDoList(item)
            }

            override fun shouldFetch(data: List<ResponseTodo>?): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<List<ResponseTodo>> {
                return db.toDoDao().getSortedListByName()
            }

            override fun createCall(): LiveData<ApiResponse<List<ResponseTodo>>> {
                return apiService.getToDoList()
            }

        }.asLiveData()
    }

    fun getSortedListStatus(status: String) : LiveData<Resource<List<ResponseTodo>>> {
        return object : NetworkBoundResource<List<ResponseTodo>, List<ResponseTodo>>(appExecutors) {
            override fun saveCallResult(item: List<ResponseTodo>) {
                db.toDoDao().insertToDoList(item)
            }

            override fun shouldFetch(data: List<ResponseTodo>?): Boolean {
                return false
            }

            override fun loadFromDb(): LiveData<List<ResponseTodo>> {
                return db.toDoDao().getSortedListByStatus(status)
            }

            override fun createCall(): LiveData<ApiResponse<List<ResponseTodo>>> {
                return apiService.getToDoList()
            }

        }.asLiveData()
    }

    fun createToDo(data : NewToDo) : LiveData<Resource<ResponseTodo>> {
        return object : NetworkBoundResourceNoCache<ResponseTodo>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ResponseTodo>> {
                return apiService.createToDo(data)
            }

        }.asLiveData()
    }

    fun updateToDo(id: String, data : NewToDo) : LiveData<Resource<ResponseTodo>> {
        return object : NetworkBoundResourceNoCache<ResponseTodo>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<ResponseTodo>> {
                return apiService.updateToDo(id, data)
            }

        }.asLiveData()
    }

    fun deleteToDo(id: String) : LiveData<Resource<Any>>{
        return object : NetworkBoundResourceNoCache<Any>(appExecutors) {
            override fun createCall(): LiveData<ApiResponse<Any>> {
                return apiService.deleteToDo(id)
            }

        }.asLiveData()
    }

}